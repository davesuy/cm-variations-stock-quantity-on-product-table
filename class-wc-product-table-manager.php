// IGNORE THIS IGNORE THIS! At the moment Its working without edting the plugin using this made a function on the functions.php file


// This changes is within plugin of woocommerce-product-table/includes/class-wc-product-table-hook-manager.php

// Line 361 or Starting the function of  wc_dropdown_variation_attribute_options


						// wc_dropdown_variation_attribute_options( array(
						// 	'options'			 => $options,
						// 	'attribute'			 => $attribute_name,
						// 	'product'			 => $product,
						// 	'selected'			 => $selected,
						// 	'show_option_none'	 => WCPT_Util::get_attribute_label( $attribute_name, $product )
						// ) );

						$args = array(
							'options'			 => $options,
							'attribute'			 => $attribute_name,
							'product'			 => $product,
							'selected'			 => $selected,
							'show_option_none'	 => WCPT_Util::get_attribute_label( $attribute_name, $product )
						);

						$options = $args['options']; 
		   				$product = $args['product']; 
					    $attribute = $args['attribute']; 
					    $name = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute ); 
					    $id = $args['id'] ? $args['id'] : sanitize_title( $attribute ); 
					    $class = $args['class']; 
					    $show_option_none = $args['show_option_none'] ? true : false; 
					    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options. 
					 
					    if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) { 
					        $attributes = $product->get_variation_attributes(); 
					        $options = $attributes[ $attribute ]; 
					    } 
					 
					    $html = '<select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">'; 
					    $html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>'; 
					 
					    if ( ! empty( $options ) ) { 
					        if ( $product && taxonomy_exists( $attribute ) ) { 
					            // Get terms if this is a taxonomy - ordered. We need the names too. 
					            $terms = wc_get_product_terms( $product->get_id(), $attribute, array( 'fields' => 'all' ) ); 

					            $product = new WC_Product_Variable( $product->get_id() );
								$variations = $product->get_available_variations();

				

								$i = 0;
					 
					            foreach ( $terms as $term ) {

						            $variation_obj = new WC_Product_variation($variations[$i]['variation_id']);				  
						            $is_in_stock = $variation_obj->get_stock_status();
						            $display_price = $variation_obj->get_price_html();

						            $stock_quantity = $variation_obj->get_stock_quantity();

						            $stock_quantity_output = "| out of stock";

						            if( $is_in_stock == 'instock') {

						            	$stock_quantity_output =  '| '.$stock_quantity. ' in stock';

						            }

						            $display_price_output = "";

						            if(!empty($display_price)) {

						            	 $display_price_output =  '| '.$display_price;

						            }
									

					                if ( in_array( $term->slug, $options ) ) { 
					                    $html .= '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ).' '.$display_price_output.' '.$stock_quantity_output.'</option>'; 


					                } 

					                $i++;
					            } 

					            //$html .= '<pre>'.print_r($display_price, true).'</pre>';
					     
					        } else { 
					            foreach ( $options as $option ) { 
					                // This handles < 2.4.0 bw compatibility where text attributes were not sanitized. 
					                $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false ); 
					                $html .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</option>'; 
					            } 
					        } 
					    } 
					 
					    $html .= '</select>'; 
					 
					    echo $html;
